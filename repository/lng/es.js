﻿var page_text_es={
	

	// Errores  ------------------------------------
	error_session:"Sesión no iniciada o caducada.",
	no_param:"Parámetros de función no compatibles.",
	error_server:"Problema desconocido en el servidor.",
	not_yours:"El recurso no pertenece al usuario activo.",
	no_project:"No existe el proyecto en el sistema.",
	no_content:"No hay contenido para esta sección.",
	no_step:"No existe la sección en el proyecto.",
	error_name:"No existe el proyecto en el sistema.",
	error_upload:"No se ha podido subir el archivo/imagen.",
	error_func:"La función no está habilitada en el servidor.",
	no_user:"No existe el usuario en el sistema.",
	error_user:"El nombre de usuario ya está siendo utilizado.",
	error_email:"El email ya está siendo utilizado.",
	error_projects:"No se han podido obtener los proyectos.",
	error_project:"No se encuentra el proyecto en el sistema.",
	error_project_data:"El nombre de proyecto ya está siendo utilizado.",

	// Modal
	modal_lbl_error:"Se ha producido un error:",
	modal_lbl_info:"Mensaje:",
	modal_btn_accept:"Aceptar",

	// Login ---------------------------------------
	login_lbl_name:"Nombre",
	login_lbl_email:"Email",
	login_lbl_password:"Contraseña",
	login_lbl_repassword:"Repite contraseña",
	login_btn_login:"Iniciar sesión",
	login_btn_register:"Registrarse",
	login_lbl_recover:"¿Has olvidado la contraseña? Introduce el email y pulsa ",
	login_link_recover:"aquí",
	login_lbl_signup:"¿No tienes cuenta? ",
	login_link_signup:"Regístrate",
	login_lbl_signin:"¿Ya tienes cuenta? ",
	login_link_signin:"Inicia sesión",

	login_error_empty:"Faltan datos.",
	login_error_email:"El email no tiene un formato correcto.",
	login_error_user:"Usuario/contraseña incorrectos.",
	login_error_server:"No se ha podido verificar la identidad.<br/><br/>Inténtalo de nuevo y, si vuelve a fallar, eníanos un email.",

	register_ok:"Te hemos enviado un correo con la contraseña de accceso.<br/><br/>Mira en la bandeja de spam si no lo recibes en unos minutos.",

	login_error_recover:"Error al inciar proceso de recuperación.",
	
	login_error_recover_email:"Correo de recuperación no válido.",
	login_recover_ok:"Nueva contraseña enviado al correo.",
	// ---------------------------------------------

	// General --------------------------------------
	user_option_projects:"Editar cuenta",
	user_option_account:"Editar cuenta",
	user_option_signout:"Cerrar sesión",
	user_option_signin:"Iniciar sesión",
	user_option_signup:"Registrarse",
	// ---------------------------------------------

	// Index --------------------------------------
	filter_keyword:"Palabra clave",
	filter_category:"Categoría",
	filter_category_all:"Todas",
	filter_type:"Tipo",
	filter_type_original:"Originales",
	filter_type_all:"Oiginales y derivaciones",
	filter_order:"Ordenar por",
	filter_sort_popular:"Más populares",
	filter_sort_forks:"Más derivaciones",
	filter_sort_date:"Últimos",
	btn_filter:"Aplicar",
	error_loading_projects:"Ha habido un problema al cargar los proyectos.",
	btn_load_retry:"Reintentar",
	btn_load_projects:"Mostrar más proyectos",
	// ---------------------------------------------

	// Project  ------------------------------------
	project_loading_error:"El proyecto no existe o ha sido eliminado del sistema.",
	step_loading_error:"El contenido no se encuentra.",
	lbl_option_fork:"Puedes crear un proyecto nuevo a partir de éste.",
	btn_option_fork:"Clonar proyecto",
	lbl_option_share:"Comparte el proyecto.",
	lbl_option_report:"Si crees que el proyecto no debería estar publicado.",
	btn_option_report:"Denunciar",
	btn_more_projects:"Más proyectos del autor",
		
	// ---------------------------------------------

	// Edit Account  -------------------------------
	lbl_account_name:"Nombre",
	lbl_account_email:"Email",
	lbl_account_password:"Contraseña <small>(No la rellenes si no deseas cambiarla)</small>",
	lbl_account_repassword:"Repite contraseña",
	lbl_account_info:"Sobre ti <small>(máximo 200 carácteres)</small>",
	btn_account_image:"Cambiar imagen",
	hint_account_user:"Es el nombre público que aparecerá en los proyectos. Sólo letras, números, . y _",
	hint_account_image:"Formato 1:1<br/>Recomendamos mínimo:<br/> 250 x 250px",
	btn_account_save:"Grabar datos",

	btn_project_save:"Grabar cambios",
	btn_project_remove:"Eliminar proyecto",
	btn_project_publish:"Publicar proyecto",

	lbl_project_delete_confirm:"Teclea DELETE para confirmar que quieres eliminar el proyecto y todo su conetnido.",
	result_project_deleted:"Proyecto eliminado correctamente",

	result_account_updated:"Datos actualizados correctamente.",
	error_account_empty:"Faltan datos por rellenar.",
	error_account_email:"Email no tiene formato válido.",
	error_account_passwords:"Las contraseñas no coinciden.",

	error_project_empty:"Faltan datos por rellenar",

	lbl_project_title:"Título",
	lbl_project_name:"Nombre identificativo <small>(utilizado para URL amigable)</small>",
	lbl_project_category:"Categoría",
	lbl_project_info:"Descripción breve <small>(máximo 200 carácteres)</small>",
	option_category_0:"MESH networks y el futuro de internet", 
	option_category_1:"Herramientas tecnológicas libres y éticas",
	option_category_2:"Seguridad y privacidad",
	option_category_3:"Metodologías y prácticas de diseño",
	option_category_4:"Diseño 2D y 3D",
	option_category_5:"Cortadora de vinilo y laser",
	option_category_6:"Electrónica y programación",
	option_category_7:"Escaneo e impresión 3D",
	option_category_8:"Arte y tecnolgía",

	title_new_project:"Sin título",
	title_new_step:"Sin título",

	error_step_empty:"Faltan datos por rellenar.",
	result_step_updated:"Actualizado ",
	result_step_error:"Error al actualizar: ",
	result_step_updating:"Grabando...",

	lbl_step_delete_confirm:"Teclea DELETE para confirmar que quieres eliminar contenido.",

	lbl_name_step:"Título de contenido",
	lbl_content_step:"Contenido",
	lbl_menu_account:"Cuenta",
	lbl_menu_projects:"Proyectos",
	lbl_menu_steps:"Contenido",
	btn_edit_info:"Información",
	btn_edit_account:"Datos de usuario",
	btn_save_step:"Guardar cambios",
	btn_remove_step:"Eliminar contenido",

	question_dont_save:"Has realizado cambios que no se han grabado ¿seguro que deseas continuar y perder las modificaciones?",

	edit_loading_error:"No se han podido cargar los datos.<br/>Inténtalo de nuevo y si contínúa el problema, por favor, envíanos un email.",
	edit_session_error:"<a href='login.html'>Inicia sesión</a> para acceder a los datos de la cuenta y proyectos."
	// ---------------------------------------------

};
