﻿function loadDone()
{
	translate_page(page_lng);
	$(".link_lng").click(function()
	{
		translate_page($(this).attr("data-lng"));
		return(false);
	});
}

$(document).ready(function()
{		
	if(page_vars["mode"])
	{
		if(page_vars["mode"]=="admin")
		{
			$(".no_admin").hide();
		}
	}
	if(window.location.hash=="#signup")
		$(".set_signin").toggleClass("hide");
	else 
		$(".set_signup").toggleClass("hide");

	$("#btn_login").click(login);
	$("#btn_register").click(register);

	$(".btn_change").click(function()
	{
		$(".login_error").text("");
		$(".set_signup").toggleClass("hide");
		$(".set_signin").toggleClass("hide");
		return(false);
	});

	$('.login_field').keypress(function(e){
      if(e.keyCode==13) $('#btn_login').click();
	});
	if($("#btn_recover").length)
	{
		$("#btn_recover").click(function()
		{
			$(".login_error").hide();
			$(".login_error").removeClass("error_no");
			var params={"function": "recoverPassword", "email":$("#login_name").val()};
			params["lng"]=page_lng;
			load_json_post(params,function(data)
			{
				if(data!=false)
				{
					if(data.result=="ok")
					{
						$(".login_error").text(page_text["login_recover_ok"]);
						$(".login_error").addClass("error_no");
						$(".login_error").show();	
						return;
					}else{
						if(data.type=="error_email")
						{
							$(".login_error").text(page_text["login_error_email"]);
							$(".login_error").show();		
							return;
						}
					}
				}
				$(".login_error").text(page_text["login_error_recover"]);
				$(".login_error").show();		
			});
			return(false);
		});
	}
});

function is_server()
{
	if(document.URL.substring(0,4)=="http") return(true);
	return(false);
}

function login()
{

	$(".login_error").removeClass("error_no");

	if((!$("#login_email").val()) ||
		(!$("#login_password").val()))
		{
			$(".login_error").html(page_text["login_error_empty"]);
			return(false);
		}
	if(!email_isvalid($("#login_email").val()))
	{
			$(".login_error").html(page_text["login_error_email"]);
			return(false);
	}

	var params={"function": "signin", "user_name":$("#login_email").val(), 
									 "user_password":$("#login_password").val(),
									 "lng":page_lng};
	params.session=sessionStorage["session"];
	/*var page="index.php";
	if(page_vars["mode"])
		if(page_vars["mode"]=="admin")
		{
			 params["mode"]="admin";
			 page="admin.php";
		}*/
	load_json_post(params,function(data)
	{
		console.log(data);
		if(data!=false)
		{
			if(data.result=="ok")
			{
				sessionStorage["session"]=data.session;
				window.location.replace(data.page);	
				return;
			}
			$(".login_error").html(page_text["login_error_user"]);
			return;
		}
		$(".login_error").html(page_text["login_error_server"]);
	});
	return(false);
}
function register()
{
	$(".login_error").removeClass("error_no");

	if((!$("#login_name").val()) ||
		(!$("#login_email").val()))
		{
			$(".login_error").html(page_text["login_error_empty"]);
			return(false);
		}
	if(!email_isvalid($("#login_email").val()))
	{
				$(".login_error").html(page_text["login_error_email"]);
				return(false);
	}
	
	var params={"function": "signup", "user_name":$("#login_name").val(), 
									 "user_email":$("#login_email").val(),
									 "lng":page_lng};
	
	load_json_post(params,function(data)
	{
		if(data!=false)
		{
			if(data.result=="ok")
			{
				$(".login_error").addClass("error_no");
				$(".login_error").html(page_text["register_ok"]);
				$(".set_signup").toggleClass("hide");
				$(".set_signin").toggleClass("hide");
			}else{
				$(".login_error").html(page_text[data.type]);
				/*switch(data.type)
				{
					case "error_user":
						$(".login_error").html(page_text["register_error_name"]);
						break;
					case "error_email":
						$(".login_error").html(page_text["register_error_email"]);
						break;
					default:
						$(".login_error").html(page_text["register_error_server"]);
				}*/
			}
		}else 
			$(".login_error").html(page_text["error_server"]);
	});
	return(false);

}