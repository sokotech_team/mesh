var page_vars=getUrlVars();
var url_parts;
if(Object.keys(page_vars).length==0)
		url_parts=window.location.pathname.split("/");
var page_lng="es";
if("site_lng" in localStorage) page_lng=localStorage["site_lng"];

var page_text;

//var rest_server="http://localhost/soko_api/htdocs/mesh/code.php";
var rest_server="https://api.sokodev.tech/mesh/code.php";
$.ajaxSetup({
	crossDomain: true,
	xhrFields: {
		withCredentials: true
	}
});

function getUrlVars() 
{
	var vars = {};
	var parts = window.location.href.replace(/[?&#]+([^=&#]+)=([^&#]*)/gi, function(m,key,value) 
	{
		vars[key] = value;
	});
	return(vars);
}

var includes_loaded=false;
var page_loaded=false;

function loadHTML(items, i)
{
	$.get($(items[i]).attr("include-HTML")+"?"+new Date().getTime())
		.done(function(data)
		{
    			$(items[i]).html(data);
				i++;
				if(i<items.length)
					loadHTML(items,i);
				else 
				{
					includes_loaded=true;
					if(page_loaded) loadDone();
				}
		});
}

function includeHTML()
{
  var imports=$("[include-HTML]");
  if(imports.length>0) loadHTML(imports,0); 
}
  

function email_isvalid(email)
{
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z]{2,4})+$/;
  return(regex.test(email));
}


function load_get(file,nocache,data_type,func)
{
	if(nocache) file+="?"+new Date().getTime();
	$.get(file, function(data){
		func(data); //JSON.parse($.trim(data)));
	},data_type)
	.fail(function() {
		func(false);
	});
}

function load_json(file,nocache,func)
{
	if(nocache) file+="?"+new Date().getTime();
	fetch(file)
     .then(function(response){
			 return response.json().then(function(data)
			 {
			 		func(data);	 
		    });
     }).catch(function(error){ 
   		console.log(error); 
		func(false);
     		});   
}

function load_json_get(file,nocache,func)
{
	if(nocache) file+="?"+new Date().getTime();
	$.get(file, function(data){
		func(data); //JSON.parse($.trim(data)));
	},"json")
	.fail(function() {
		func(false);
	});
}

function load_script_get(file,nocache,func)
{
	if(nocache) file+="?"+new Date().getTime();
	$.get(file, function(data){
		func(data); //JSON.parse($.trim(data)));
	},"script")
	.fail(function() {
		func(false);
	});
}

function load_json_post(params,func)
{
	$.post(rest_server, params,function(data){
		func(data); 
	},"json")
	.fail(function(jqXHR, textStatus, errorThrown) {
		console.log(jqXHR.responseText);
		func(false);
	});
}

function load_text_post(params,func)
{
	$.post(rest_server, params,function(data){
		func(data); 
	});
}

function load_doc(doc,page,func)
{
 	var url = "https://spreadsheets.google.com/feeds/list/" + doc + "/"+page+"/public/values?alt=json";
	fetch(url).then(function(response){
			 return response.json().then(function(data)
			 {
			 		func(data);	 
		    });
     }).catch(function(error){ 
			func(false);   		
   		console.log(error); 
     		});   
}

function translate_page(lng)
{
	localStorage["site_lng"]=lng;
	page_lng=lng;
	page_text=window["page_text_"+page_lng];

	var elements=$("[aria-lng]");
	for(i=0;i<elements.length;i++)
	{
		var text=$(elements[i]).attr("aria-lng");
		if(page_text[text])
			$(elements[i]).html(page_text[text]);
	}
	
	var elements=$("[aria-select]");
	for(i=0;i<elements.length;i++)
	{
		var select=$(elements[i]).attr("aria-select");
		if(page_text[select])
		{
			$(elements[i]).html("");
			var text=page_text[select];
			var options=text.split(",");
			for(o=0;o<options.length;o++)
			{
				var op=options[o].split("/");
				$(elements[i]).append('<option value="'+op[1]+'">'+op[0]+'</option');
			}
			$(elements[i]).val("");
		}
	}	
	
	var elements=$("[aria-holder]");
	for(i=0;i<elements.length;i++)
	{
		var text=$(elements[i]).attr("aria-holder");
		if(page_text[text])
			$(elements[i]).attr("placeholder",page_text[text]);
	}
	
}

$(document).ready(function()
{	
	page_loaded=true;
	page_text=window["page_text_"+page_lng];
	if(($("[include-HTML]").length==0) || (includes_loaded)) loadDone();	
	return;
});

