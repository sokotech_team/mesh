var vars=getUrlVars();

var edit_projects;

var edit_timer=0;
var edit_last_updated=0;

function getUrlVars() 
{
	var vars = {};
	var parts = window.location.href.replace(/[?&#]+([^=&#]+)=([^&#]*)/gi, function(m,key,value) 
	{
		vars[key] = value;
	});
	return(vars);
}

function show_modal(type, error)
{
	var text=type;
	if(page_text[type])
		text=page_text[type];
	$("#modal_msg .modal_body").text(text);
	if(error)
		$("#modal_msg .modal_header").text(page_text["modal_lbl_error"]);
	else
		$("#modal_msg .modal_header").text(page_text["modal_lbl_info"]);
	$("#modal_msg .modal_btn_accept").off("click");
	$("#modal_msg .modal_btn_accept").on("click",function()
	{
		$("#modal_msg").addClass("hide");
		return(false);
	});
	
	$("#modal_msg").removeClass("hide");
}
function show_lng(lng)
{
	$(".lng").hide();
	$(".lng_"+lng).show();
	var i=$("[placeholder_es]");
	for(var a=0;a<i.length;a++)
		$(i[a]).attr("placeholder",$(i[a]).attr("placeholder_"+lng));
	page_lng=lng;
}

function email_isvalid(email)
{
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z]{2,4})+$/;
  return(regex.test(email));
}

function show_project(target,data)
{
	//console.log(data);
	$(target).find(".project_title").text(data.title);
	$(target).find(".project_info").text(data.info);

	$(target).find(".author_name").text(data["author"].name);
	$(target).find(".author_user").text(data["author"].user);	
	$(target).find(".author_info").html(data["author"].info);
	$(target).find(".author_image").attr("src",data["author"].image);
	$(target).find(".author_link").attr("href",data["author"].link);
	$(target).find(".author_link").text(data["author"].link);

	$(target).find(".stats_views").text(data.views);
	$(target).find(".stats_forks").text(data.forks);
	$(target).find(".stats_category").text(page_text["option_category_"+data.category]);
	$(target).find(".stats_date").text(data.date);

	if(data.image)
		$(target).find(".project_image").css("background-image","url('"+data.image+"')");

	for(a=0;a<data.steps.length;a++)
	{
		var step=$("#dummy .step_item").clone();

		//$(step).attr("aria-content",data.steps[a].content);
		$(step).attr("aria-stepid",data.steps[a].id);
		$(step).attr("aria-projectid",data.id);
		
		$(step).text((a+1)+". "+data.steps[a].title);
		$(target).find(".project_steps").append(step);
	}
	$(".project_steps [aria-stepid]").first().addClass("step_selected");
	$(".project_steps").find(".step_item").click(show_step);
	load_project_content($(".project_steps [aria-stepid]").first()[0]);
}


function load_project_content(ctl)
{
	$(".step_error").addClass("hide");
	$(".step_no_error").removeClass("hide");
	
	$(".project_step_content").css("filter","blur(4px)");
	setTimeout(function()
	{
		var params={"function": "getstep", "project_id":$(ctl).attr("aria-projectid"),
				"step_id":$(ctl).attr("aria-stepid"), "lng":page_lng};
		load_json_post(params,function(data)
		{
			if(data!=false)
			{
				if(data.result=="ok")
				{
					$(".project_step_content").html(data.step_content);
					if($("trix-editor").length) $("trix-editor").attr("aria-changed",0);
				}else{
					$(".step_error").removeClass("hide");
					$(".step_no_error").addClass("hide");
					$(".project_step_content").html("");
				}
			}else{
				$(".step_error").removeClass("hide");
				$(".step_no_error").addClass("hide");
				$(".project_step_content").html("");
			}
			$(".project_step_content").css("filter","blur(0px)");
			$(".step_selected").removeClass("step_selected");
		
			$("[aria-stepid='"+$(ctl).attr("aria-stepid")+"']").addClass("step_selected");
			if($(ctl).parent().is("[aria-scroll]"))
			{
				$('html, body').animate({
				scrollTop: ($($(ctl).parent().attr("aria-scroll")).offset().top)
					},500);
			}
		});
	},250);
}

function show_step()
{
	if(!$(this).hasClass("step_selected"))
	{
		if($(this).is("[aria-stepid]"))
			load_project_content(this);
	}
	return(false);
}

function show_account(ctl, data)
{
	// usuario
	$(ctl).find("#user_name").val(data.user.name);
	$(ctl).find("#user_info").val(data.user.info);
	$(ctl).find("#user_email").val(data.user.email);
	$(ctl).find("#user_user").val(data.user.user);
	if(data.user.image)
		$(ctl).find("#user_image").css("background-image","url('"+data.user.image+"')");

	//console.log(data);

	// proyectos
	var prjs=data.projects;
	for(a=0;a<prjs.length;a++)
	{
		var prj=$("#dummy .project_item").clone();
		$(prj).find(".project_title").text(prjs[a].title);
		$(prj).find(".project_title").attr("aria-project",prjs[a].id);
		
		var btn=$(prj).find(".btn_add_step");
		$(btn).attr("aria-project",prjs[a].id);

		edit_projects[prjs[a].id]={"title":prjs[a].title,
									"info":prjs[a].info,
									"image":prjs[a].image,
									"name":prjs[a].name,
									"category":prjs[a].category};

		for(b=0;b<prjs[a].steps.length;b++)
		{
			var step=$("#dummy .step_item").clone();
			$(step).attr("aria-stepid",prjs[a].steps[b].id);
			$(step).attr("aria-projectid",prjs[a].id);
			//$(step).attr("aria-content",prjs[a].steps[b].content);
			
			$(step).text(prjs[a].steps[b].title);
			$(prj).find(".project_steps").append(step);
		}
		$(ctl).find(".menu_projects").append(prj);
	}
	// expandir instrucciones
	$(ctl).find(".btn_project_open").click(open_steps);

	$(ctl).find(".edit_menu_item").click(click_edit_option);

	$(".btn_add_step").click(add_new_step);
}

function open_steps()
{
	if($(this).has("[aria-expand]"))
	{
		$(this).parent().find($(this).attr("aria-expand")).toggleClass("hide");
		$(this).toggleClass("btn_opened");
	}
	return(false);
}

function click_edit_option()
{
	var ctl=$(".edit_menu");
	var tab=$(this).attr("aria-tab");
	if($(tab).hasClass("hide"))
	{
		$(".edit_tab").addClass("hide");
		$(tab).removeClass("hide")
	}

	if(!$(this).hasClass("edit_menu_item_selected"))
	{
		$(ctl).find(".edit_menu_item_selected").removeClass("edit_menu_item_selected");
		$(this).addClass("edit_menu_item_selected");

		$(".step_result").html("");
		$(".project_result").html("");
		$(".account_result").html("");
		$(".input_error").removeClass("input_error");
	}else{ 
		return(false);
	}
	if($(this).is("[aria-stepid]"))
	{
		$("#step_title").val($(this).text());
		load_project_content(this);
	}
	if($(this).is("[aria-project]"))
	{
		$(".project_btn").removeAttr("disabled");
		var i=$(this).attr("aria-project");
		$("#project_title").val($(this).text());
		$("#project_name").val(edit_projects[i].name);
		$("#project_category").val(edit_projects[i].category);
		if(edit_projects[i].image)
			$("#project_image").css("background-image","url('"+edit_projects[i].image+"')");
		else
			$("#project_image").css("background-image","");
		$("#project_info").val(edit_projects[i].info);
		$(".project_btn").attr("aria-target",i);
	}
	return(false);
}

function show_projects(grid,data)
{
	$(grid).html("");
	for(var a=0;a<data.length;a++) 
	{
		
		var prj=$("#dummy .project_item").clone();

		prj.find(".project_title").text(data[a].title);
		prj.find(".project_title").attr("href","project.html?name="+data[a].name);
		
		prj.find(".project_author").text(data[a].author);
		prj.find(".project_info").text(data[a].info);
		prj.find(".project_date").text(data[a].date);

		prj.find(".project_views").text(data[a].views);
		prj.find(".project_forks").text(data[a].forks);
		prj.find(".project_category").text(page_text["option_category_"+data[a].category]); 

		if(data[a].image)
			prj.find(".project_image").css("background-image","url('"+data[a].image+"')");
		
		$(grid).append(prj);
		 //prj.find(".project_image").height(prj.find(".project_image").width()/2);
	}
}

function add_new_step()
{
	var prj=$(this).attr("aria-project");
	var params={"function": "addstep", "project_id":prj,
				"step_name":page_text["title_new_step"], "lng":page_lng};
	params["session"]=sessionStorage["session"];
	load_json_post(params,function(data)
	{
		if(data!=false)
		{
			if(data.result=="ok")
			{
				var parent=$(".menu_projects").find("[aria-project='"+prj+"']").parent().find(".project_steps");
				var step=$("#dummy .step_item").clone();
				$(step).text(page_text["title_new_step"]);
				//$(step).attr("aria-content",data.step_id);
				$(step).attr("aria-stepid",data.step_id);
				$(step).attr("aria-projectid",prj);
				$(parent).append(step); 

				$(".edit_menu").find(".edit_menu_item").off("click");
				$(".edit_menu").find(".edit_menu_item").click(click_edit_option);
				$(".menu_projects").find("[aria-stepid='"+data.step_id+"']").trigger("click");
				
			}else{
				show_modal(data.type,true);
			}
		}else{
			show_modal("error_server",true);
		}
	});
	return(false);
}

function modify_step()
{
	var step=$(".step_item.edit_menu_item_selected");
	
	//$(".step_result").html("");
	$(".input_error").removeClass("input_error");
	$(".step_result").removeClass("lbl_result_error");

	var params={"function": "modifystep", "project_id":$(step).attr("aria-projectid"),
			"step_id":$(step).attr("aria-stepid"), "lng":page_lng};
	params["session"]=sessionStorage["session"];
	//var form=".step_form";

	if($("#step_title").val()=="")
		$("#step_title").addClass("input_error");

	if($("trix-editor").html()=="") 
		$("trix-editor").addClass("input_error");

	if($(".input_error").length)
	{
		$(".step_result").addClass("lbl_result_error");
		$(".step_result").html(page_text["error_step_empty"]);
		return(false);
	}

	params["step_title"]=$("#step_title").val();
	
	//var document = $("trix-editor")[0].editor.getDocument()
	//console.log(JSON.stringify($("trix-editor")[0].editor));
	//console.log(document.toString());

	params["step_content"]=$("trix-editor")[0].value;
	//params["step_content"]=JSON.stringify($("trix-editor")[0].editor);

	load_json_post(params,function(data)
	{
		if(data!=false)
		{
			if(data.result=="ok")
			{
				var today = new Date();
				$(".step_result").html(page_text["result_step_updated"]+
				today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds());
				$(".project_steps").find(".step_selected").text($("#step_title").val());
				return;
			}else{
				$(".step_result").html(page_text["result_step_error"]+page_text[data.type]);
			}
		}else
			$(".step_result").html(page_text["result_step_error"]+page_text["error_server"]);
		$(".step_result").addClass("lbl_result_error");

	});
	return(false);
}

function remove_step()
{
	if (prompt(page_text["lbl_step_delete_confirm"])!="DELETE") return(false);
	
	var step=$(".step_item.edit_menu_item_selected");
	$(".step_result").html("");
	$(".step_result").removeClass("lbl_result_error");

	var params={"function": "removestep", "project_id":$(step).attr("aria-projectid"),
			"step_id":$(step).attr("aria-stepid"), "lng":page_lng};
	params["session"]=sessionStorage["session"];
	load_json_post(params,function(data)
	{
		if(data!=false)
		{
			if(data.result=="ok")
			{
				$(".menu_projects").find(".project_title[aria-project='"+$(step).attr("aria-projectid")+"']").trigger("click");
				$(step).remove();
				$("trix-editor")[0].editor="";
				return;
			}else{
				show_modal(data.type,true);
			}
		}else
			show_modal("error_server",true);
	});
	return(false);
}

function save_account()
{
	$(".account_result").html("");
	$(".input_error").removeClass("input_error");
	$(".account_result").removeClass("lbl_result_error");

	var params={"function": "modifyuser", "lng":page_lng};
	params["session"]=sessionStorage["session"];
	var form=".user_form";

	var vals=$(form).find("input, textarea");
	for(a=0;a<vals.length;a++)
	{
		if($(vals[a]).is("[required]") && ($(vals[a]).val()==""))
			$(vals[a]).addClass("input_error");
		if($(vals[a]).val()!="")
			params[$(vals[a]).attr("id")]=$(vals[a]).val();
	}
	if($(".input_error").length)
	{
		$(".account_result").addClass("lbl_result_error");
		$(".account_result").html(page_text["error_account_empty"]);
		return(false);
	}

	if(!email_isvalid($("#user_email").val()))
	{
		$("#user_email").addClass("input_error");
		$(".account_result").addClass("lbl_result_error");
		$(".account_result").html(page_text["error_account_email"]);
		return(false);
	}

	if($("#user_password").val()!="")
	{
		if($("#user_password").val()!=$("#user_repassword").val())
		{
			$("#user_password").addClass("input_error");
			$("#user_repassword").addClass("input_error");
			$(".account_result").addClass("lbl_result_error");
			$(".account_result").html(page_text["error_account_passwords"]);	
			return(false);
		}
	}
	if($("#user_image").is("[aria-img]"))
	{
		var img=$("#user_image").css("background-image");
		params["user_image"]=img.substring(5,img.length-2);
	}
	load_json_post(params,function(data)
	{
		if(data!=false)
		{
			if(data.result=="ok")
			{
				$(".account_result").html(page_text["result_account_updated"]);
				$(".nav_user_img").attr("src",data.user_image);
				$(".user_popup_name").text(data.user_name);
			}else{
				show_modal(data.type,true);
			} 
		}else{ 
			show_modal("error_server",true);
		}
	});
	return(false);
}

function new_project()
{
	var params={"function": "addproject", "project_name":page_text["title_new_project"],
				 "lng":page_lng};
	params["session"]=sessionStorage["session"];

	load_json_post(params,function(data)
	{
		if(data!=false)
		{
			if(data.result=="ok")
			{
				var prj=$("#dummy .project_item").clone();
				$(prj).find(".project_title").text(page_text["title_new_project"]);
																	
				$(prj).find(".project_title").attr("aria-project",data.project_id);
				$(".menu_projects").append(prj); 

				edit_projects[data.project_id]={"title":page_text["title_new_project"],
											"name":data.project_id,"category":"0"};

				$(".edit_menu").find(".edit_menu_item").off("click");
				$(".edit_menu").find(".edit_menu_item").click(click_edit_option);
				
				$(".menu_projects").find(".project_title[aria-project='"+data.project_id+"']").trigger("click");

				$(".edit_menu").find(".btn_project_open").off("click");
				$(".edit_menu").find(".btn_project_open").click(open_steps);

				$(prj).find(".btn_add_step").attr("aria-project",data.project_id);

				$(".edit_menu").find(".btn_add_step").off("click");
				$(".edit_menu").find(".btn_add_step").click(add_new_step);
			}else{
				show_modal(data.type,true);
			}
		}else{
			show_modal("error_server",true);
		}
	});
	return(false);
}

function remove_project()
{
	if($(this).is("[disabled]")) return(false);		
	if (prompt(page_text["lbl_project_delete_confirm"])=="DELETE")
	{
		var id=$(this).attr("aria-target");
		var params={"function": "removeproject", "project_id":id, "lng":page_lng};
		params["session"]=sessionStorage["session"];
		load_json_post(params,function(data)
		{
			if(data!=false)
			{
				if(data.result=="ok")
				{
					$("[aria-project='"+id+"']").parent().remove();
					$("#project_image").css("background-image","");
					$(".project_form input").val("");
					$(".project_form textarea").val("");
					$(".project_btn").attr("disabled", "");
					$(".project_result").html(page_text["result_project_deleted"]);	
					return;		
				}
			}
			show_modal(data.type,true);
		});

	}
	return(false);
}

function save_project()
{
	$(".project_result").html("");
	$(".input_error").removeClass("input_error");
	$(".project_result").removeClass("lbl_result_error");

	var target=$(this).attr("aria-target");

	var params={"function": "modifyproject", "project_id":$(this).attr("aria-target"),"lng":page_lng};
	params["session"]=sessionStorage["session"];
	var form=".project_form";

	var vals=$(form).find("input, textarea, select");
	for(a=0;a<vals.length;a++)
	{
		if($(vals[a]).is("[required]") && ($(vals[a]).val()==""))
			$(vals[a]).addClass("input_error");
		if($(vals[a]).val()!="")
			params[$(vals[a]).attr("id")]=$(vals[a]).val();
	}
	if($(".input_error").length)
	{
		$(".project_result").addClass("lbl_result_error");
		$(".project_result").html(page_text["error_project_empty"]);
		return(false);
	}

	if($("#project_image").is("[aria-img]"))
	{
		var img=$("#project_image").css("background-image");
		params["project_image"]=img.substring(5,img.length-2);
	}
	
	load_json_post(params,function(data)
	{
		if(data!=false)
		{
			if(data.result=="ok")
			{
				$(".project_result").html(page_text["result_account_updated"]);
				$(".project_title[aria-project='"+target+"']").text($(form).find("#project_title").val());
				edit_projects[target]["title"]=$(form).find("#project_title").val();
				edit_projects[target]["name"]=$(form).find("#project_name").val();
				edit_projects[target]["category"]=$(form).find("#project_category").val();
				edit_projects[target]["info"]=$(form).find("#project_info").val();
				edit_projects[target]["image"]=data.project_image;
			}else{
				show_modal(data.type,true);
			} 
		}else{ 
			show_modal("error_Server",true);
		}
	});
	return(false);
}

function publish_project()
{

	return(false);
}

function loadDone()
{
	translate_page(page_lng);

	var params={"function": "getuser", "lng":page_lng};
	params["session"]=sessionStorage["session"];
	load_json_post(params,function(data)
	{
		console.log(data);
		if(data!=false)
		{
			if(data.result=="ok")
			{
				//$(".nav_user_img").attr("src",data.userimg);
				$(".nav_user_img").css("background-image","url('"+data.userimg+"')");
				$(".user_popup_signed").removeClass("hide");
				$(".user_popup_signed").addClass("user_popup_active");
				$(".user_popup_anonymous").addClass("hide");
				$(".user_popup_name").text(data.username);
			}
		}
	});

	$(".btn_user").click(function()
	{
		$(".user_popup").toggle();
		return(false);
	});

	$("#btn_signout").click(function()
	{
		var params={"function": "signout", "lng":page_lng};
		load_json_post(params,function(data)
		{
			if(data!=false)
			{
				if(data.result=="ok")
				{
					sessionStorage.removeItem("session");
					window.location.href = "login.html";
				}
		}
		});
	});

 
	//show_lng(page_lng);
}

function load_picture(e)
{
	e.preventDefault();
	if(this.files.length === 0) return;
	var imageFile = this.files[0];
	
	var target=$("#photoPicker").attr("aria-target");

	var reader = new FileReader();
    reader.onload = (function(aImg){
		 return function(e) 
		 { 
			$(aImg).attr("aria-img","");
			$(aImg).css("background-image","url("+e.target.result+")");
		}; 
	})(target);
    reader.readAsDataURL(imageFile);
	
	/*
	EXIF.getData(imageFile, function ()
	{
		var orientation=1;
		if(this.exifdata.Orientation)
			orientation=this.exifdata.Orientation;
			
		var img = new Image();
		var url = window.URL ? window.URL : window.webkitURL;
		img.src = url.createObjectURL(imageFile);
		img.onload = function(e) {
			url.revokeObjectURL(this.src);
		
			var dest_width=100;			// el recuadro de destino
			var dest_height=100;
			
			var width=img.width;		// el recuadro original
			var height=img.height;
				
			var canvas = document.createElement('canvas');	
			
			
			var scale=500/width;
			dest_width=width*scale;	
			dest_height=height*scale;
				
			canvas.width=width*scale;
			canvas.height=height*scale;
		
			if((orientation==6)||(orientation==8))
			{
				dest_width=width*scale;	
				dest_height=height*scale;

				canvas.width=height*scale;
				canvas.height=width*scale;
			}
			
			var ctx = canvas.getContext("2d");
			
			switch(orientation)
			{
				case 6:
					ctx.rotate(90 * (Math.PI/180));
					ctx.translate(0,-dest_height);		
					break;
				case 3:
					ctx.rotate(180 * (Math.PI/180));
					ctx.translate(-dest_width,-dest_height);
					break;
				case 8:
					ctx.rotate(-90 * (Math.PI/180));
					ctx.translate(-dest_width,0);
			}
								
			ctx.drawImage(img, 0, 0, width, height,0,0,dest_width,dest_height);
			
			var data = canvas.toDataURL('image/jpeg',0.5);
			$(target).css("background-image","url("+data+")"); 
		};
	});	
	*/
}

function remove_file(attachment)
{
	alert(attachment);
	var step=$(".step_item.edit_menu_item_selected");
	var params={"function": "removefile", "lng":page_lng,
			"file":attachment,
			'step_id':$(step).attr("aria-stepid"),
			'project_id':$(step).attr("aria-projectid")
		};
		load_json_post(params,function(data)
		{
			if(data!=false)
			{
				if(data.result!="ok")
					show_modal(data.type,true);
			}else{
				show_modal("error_server",true);
			}
		});
}
function upload_file(attachment)
{
	var fd = new FormData(); 
	fd.append('file', attachment.file); 
	fd.append('type', attachment.file.type); 
	fd.append('function','uploadfile');
	fd.append('lng',page_lng);
	var step=$(".step_item.edit_menu_item_selected");
	fd.append('step_id',$(step).attr("aria-stepid"));
	fd.append('project_id',$(step).attr("aria-projectid"));

	var xhr = new XMLHttpRequest();
    xhr.open("POST",rest_server, true);

    xhr.upload.onprogress=function(event) {
      var progress = event.loaded / event.total * 100
	  attachment.setUploadProgress(progress)
    };

	xhr.onload = function() 
	{
		if (this.status == 200) 
		{
			//console.log(this.response);

			  var res=JSON.parse(this.response);
			  if(res.result=="ok")
			  {
				//console.log(res);
				var attributes = {
					url:res.href,
					href:res.href //+"?content-disposition=attachment"
				  }
				  attachment.setAttributes(attributes);
				  return;
			  }else{
				$("[data-trix-id='"+attachment.id+"']").remove();
				show_modal(res.type,true);
				return;
			  }
		}
		show_modal("error_server",true);
		$("[data-trix-id='"+attachment.id+"']").remove();
    };
    xhr.send(fd);
}
$(document).on("trix-initialize", function(e) 
{
	$(".content_header").append($("trix-toolbar"));
});
$(document).ready(function()
{	
	if($(".projects_grid").length)
	{
		var params={"function": "getprojects", "lng":page_lng};
		load_json_post(params,function(data)
		{
			if(data!=false)
			{
				console.log(data);
				if(data.result=="ok")
				{
					show_projects($(".projects_grid"),data.projects);
				}
			}
		});
	}
	
	if($(".project_section").length)
	{
		if(url_parts) page_vars["name"]=url_parts[url_parts.length-1];
		var params={"function": "getproject", "lng":page_lng, "project":page_vars["name"]};
		load_json_post(params,function(data)
		{
			console.log(data);
			if(data!=false)
			{
				if(data.result=="ok")
				{
					show_project($(".project_section"),data.project);
				}else{
					$(".project_error").removeClass("hide");
					$(".project_noerror").addClass("hide");
				}
			}else{
				$(".project_error").removeClass("hide");
				$(".project_noerror").addClass("hide");
			}
		});
	}

	if($(".edit_section").length)
	{
		$(document).on("trix-attachment-add", function(e) 
		{
			if(e.originalEvent.attachment.file)
				upload_file(e.originalEvent.attachment);
		});
		$(document).on("trix-attachment-remove", function(e) 
		{
			//if(e.originalEvent.attachment)
			//	remove_file(e.originalEvent.attachment.id);
		});

		$("trix-editor").on("input",function()
		{
			$("trix-editor").attr("aria-changed",1);
		});
		$(document).on("trix-change", function(e)
		{
			if($("trix-editor").attr("aria-changed")==1)
			{
				if(edit_timer!=0)
				{
					clearTimeout(edit_timer);
				}else{
					$(".step_result").removeClass("lbl_result_error");
					$(".step_result").html(page_text["result_step_updating"]);
				}
				edit_timer=setTimeout(function()
				{
					modify_step();
					$("trix-editor").attr("aria-changed",0);	
					edit_timer=0;
				},1500);
				
				//$("trix-editor").attr("aria-changed",parseInt($("trix-editor").attr("aria-changed"))+1);
			}
			return(false);
		});

		if(url_parts) page_vars["name"]=url_parts[url_parts.length-1];
		var params={"function": "getuserprojects", "lng":page_lng};
		params["session"]=sessionStorage["session"];
		load_json_post(params,function(data)
		{
			//console.log(data);
			if(data!=false)
			{
				if(data.result=="ok")
				{
					//edit_projects=data.projects;
					edit_projects=new Array();
					show_account($(".edit_section"),data);
					$(".edit_error").addClass("hide");
					$(".edit_noerror").removeClass("hide");

					//$("[warning-change]").off("click");
					/*$("[warning-change]").on("click",function(e)
					{
						if($("trix-editor").attr("aria-changed")>1)
						{
								alert("pasa");
								
						}
						$("trix-editor").attr("aria-changed",0);
					});*/
			
				}else{
					if(data.type=="error_session")
						$(".edit_error_msg").html(page_text["edit_session_error"]);
					else
						$(".edit_error_msg").html(page_text["edit_loading_error"]);
					$(".edit_error").removeClass("hide");
					$(".edit_noerror").addClass("hide");
				}
			}else{
				$(".edit_error_msg").html(page_text["edit_loading_error"]);
				$(".edit_error").removeClass("hide");
				$(".edit_noerror").addClass("hide");
			}
		});
	}
	if($(".btn_change_image").length)
	{
		$(".btn_change_image").click(function(e)
		{
			e.preventDefault();
			$("#photoPicker").attr("aria-target",$(this).attr("aria-target"));
			$("#photoPicker").trigger("click");
			return(false);
		});
	}

	if($("#photoPicker").length)
	{
		$('#photoPicker').on('change', load_picture);
	}

	if($("#btn_account_save").length)
		$("#btn_account_save").click(save_account);

	if($("#btn_add_project").length)
		$("#btn_add_project").click(new_project);

	if($("#btn_project_remove").length)
		$("#btn_project_remove").click(remove_project);
	
	if($("#btn_project_save").length)
		$("#btn_project_save").click(save_project);

	if($("#btn_step_save").length)
		$("#btn_step_save").click(modify_step);
	
	if($("#btn_step_remove").length)
		$("#btn_step_remove").click(remove_step);

	/*if($("#tiny").length)
	{
		$('textarea#tiny').tinymce({
			height: 500,
			menubar: false,
			plugins: [
			  'advlist autolink lists link image charmap print preview anchor',
			  'searchreplace visualblocks code fullscreen',
			  'insertdatetime media table paste code help wordcount'
			],
			toolbar: 'code | image | media | undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help'
		  });
	}*/

	return(false);	
});